""""
Para esta práctica vamos a creare una clase base llamada Persona que incluya como atributos el nombre, apellidos, fecha de nacimiento y DNI. 
Además tiene que tener el método __str__ para mostrar toda la información y los setters y getters de los atributos de la clase.

También vamos a implementar dos clases hijas que heredan de Persona:

    Paciente. Que tenga un atributo adicional como historial_clínico y un método que sea ver_historial_clinico que muestre el atributo. 
    Médico. Que tenga como atributos especialidad y citas. Así como un método que sea consultar_agenda que muestra todas las citas programadas. 

Como el primer ejercicio, será necesario añadir un fichero .gitlab-ci.yml con test automáticos que evalúen los métodos implementados. 
"""
class Persona:
    def __init__(self,no,ap,na,dni):
        self.nombre=no
        self.apellidos=ap
        self.nacimiento=na
        self.DNI=dni
    def get_nombre(self):
        return self.nombre
    def set_nombre(self,no):
        self.nombre=no
    def get_apellidos(self):
        return self.apellidos
    def set_apellidos(self,ap):
        self.apellidos=ap
    def get_nacimiento(self):
        return self.nacimiento
    def set_nacimiento(self,na):
        self.nacimiento=na
    def get_dni(self):
        return self.DNI
    def set_dni(self,dni):
        self.DNI=dni
    def __str__(self):
        return f"Nombre: {self.get_nombre()}\n" \
               f"Apellidos: {self.get_apellidos()}\n" \
               f"Fecha de Nacimiento: {self.get_nacimiento()}\n" \
               f"DNI: {self.get_dni()}"
class Paciente(Persona):
    def __init__(self, nombre, apellidos, fecha_nacimiento, dni, his_clin):
        super().__init__(nombre, apellidos, fecha_nacimiento, dni)
        self.historial_clinico = his_clin
    def ver_historial_clinico(self):
        return self.historial_clinico

class Medico(Persona):
    def __init__(self ,nombre, apellidos, fecha_nacimiento, dni, especialidad,agenda):
        super().__init__(nombre, apellidos, fecha_nacimiento, dni)
        self.especialidad=especialidad
        self.agenda=agenda
    def consultar_agenda(self):
        return self.agenda



