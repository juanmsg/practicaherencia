import unittest
from codigoherencia import Persona, Paciente, Medico

class TestPersona(unittest.TestCase):
    def test_str(self):
        persona = Persona("Juan", "Pérez", "01/01/1980", "12345678A")
        self.assertEqual(str(persona), "Nombre: Juan\nApellidos: Pérez\nFecha de Nacimiento: 01/01/1980\nDNI: 12345678A")

class TestPaciente(unittest.TestCase):
    def test_ver_historial_clinico(self):
        paciente = Paciente("María", "López", "02/02/1990", "98765432B", "Historial: ...")
        self.assertEqual(paciente.ver_historial_clinico(), "Historial: ...")

class TestMedico(unittest.TestCase):
    def test_consultar_agenda(self):
        medico = Medico("Dr. García", "Fernández", "03/03/1985", "23456789C", "Cardiología", ["Cita 1", "Cita 2"])
        self.assertEqual(medico.consultar_agenda(), ["Cita 1", "Cita 2"])

if __name__ == '__main__':
    unittest.main()
